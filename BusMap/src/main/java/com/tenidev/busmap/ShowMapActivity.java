package com.tenidev.busmap;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tenidev.downloader.IconDownloader;
import com.tenidev.entity.RouteItem;
import com.tenidev.entity.Stop;
//import com.tenidev.web.ImageLoader;

import java.util.ArrayList;

public class ShowMapActivity extends FragmentActivity {

    static final LatLng DEFAULT_CENTER = new LatLng(-27.478649, -58.81517);

    private GoogleMap map;

    ArrayList<Stop> busStops;
    ArrayList<RouteItem> busRoute;

    //private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_map);

        //imageLoader = new ImageLoader(this.getApplicationContext());

        ActionBar actionBar = getActionBar();
        actionBar.setTitle("Recorrido en el mapa");
        actionBar.setDisplayHomeAsUpEnabled(true);

        Bundle bundle = this.getIntent().getExtras();
        busStops = bundle.getParcelableArrayList("bus.stops");
        busRoute = bundle.getParcelableArrayList("bus.route");
        String busColor = bundle.getString("bus.color");

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapShowRoute)).getMap();

        if (map!=null){

            for(Stop itemStop: busStops){

                LatLng coords = new LatLng(itemStop.getLatitude(), itemStop.getLongitude());

                MarkerOptions markerOptions = new MarkerOptions().position(coords)
                        .title(itemStop.getDescription());

                /*if(itemStop.getIcon().isEmpty()){
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stop_standard));
                }else{
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(imageLoader.getBitmap(itemStop.getIcon(), R.drawable.ic_stop_standard)));
                }*/
                new IconDownloader(map, markerOptions).execute(itemStop.getIcon());

                //Marker markerStop = map.addMarker(markerOptions);

            }

            PolylineOptions rectOptions = new PolylineOptions();
            rectOptions.color(Color.parseColor(busColor));

            for(RouteItem routeItem: busRoute){

                LatLng coords = new LatLng(routeItem.getLatitude(), routeItem.getLongitude());

                rectOptions.add(coords);

            }

            // Get back the mutable Polyline
            Polyline polyline = map.addPolyline(rectOptions);


            // Move the camera instantly to hamburg with a zoom of 15.
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_CENTER, 15));

            // Zoom in, animating the camera.
            map.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        return SelectMenu(item);
    }

    private boolean SelectMenu(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }
    
}
