package com.tenidev.busmap;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.tenidev.adapter.ExpandableListAdapter;
import com.tenidev.downloader.util.ImageCache;
import com.tenidev.downloader.util.ImageFetcher;
import com.tenidev.entity.Route;
import com.tenidev.entity.RouteItem;
import com.tenidev.entity.Stop;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BusDetailActivity extends FragmentActivity {

    List<String> groupList = new ArrayList<String>();
    Map<String, List<? extends Route>> categoryCollection = new LinkedHashMap<String, List<? extends Route>>();;
    ExpandableListView expListView;

    private ImageFetcher mImageFetcher;

    ArrayList<Stop> busStops;
    ArrayList<RouteItem> busRoute;
    ArrayList<RouteItem> busRouteVisible = new ArrayList<RouteItem>();
    String busColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busdetail);

        try {
            initImageFetcher();

            Bundle bundle = this.getIntent().getExtras();

            String busTitle = bundle.getString("bus.title");
            String busNeighborhood = bundle.getString("bus.neighborhood");
            String busJourneyTime = bundle.getString("bus.journeyTime");
            String busDistanceTraveled = bundle.getString("bus.distanceTraveled");
            String busThumb = bundle.getString("bus.thumb");
            busColor = bundle.getString("bus.color");
            busStops = bundle.getParcelableArrayList("bus.stops");
            busRoute = bundle.getParcelableArrayList("bus.route");

            createCollection("Recorrido", busRouteVisible);
            createCollection("Paradas", busStops);

            setRouteItemsVisible();

            bundle.clear();

            TextView textView = (TextView) findViewById(R.id.busDistanceTraveled);
            textView.setText(busDistanceTraveled);

            TextView textView1 = (TextView) findViewById(R.id.busJourneyTime);
            textView1.setText(busJourneyTime);

            ImageView imageView = (ImageView) findViewById(R.id.busThumb);
            getImageFetcher().loadImage(busThumb, imageView);
            //new ImageLoader(this.getApplicationContext()).DisplayImage(busThumb, imageView, R.drawable.img_avatar_80px);

            ActionBar actionBar = getActionBar();
            actionBar.setTitle(busTitle);
            actionBar.setDisplayHomeAsUpEnabled(true);



            expListView = (ExpandableListView) findViewById(R.id.expandableListView);
            final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(
                    this, groupList, categoryCollection);
            expListView.setAdapter(expListAdapter);
            expListView.expandGroup(1);
            expListView.expandGroup(0, true);

        } catch (Exception ex) {
            Log.d("DataManagement Error", ex.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.show_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        return SelectMenu(item);
    }

    private boolean SelectMenu(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_show_map:
                Intent intent = new Intent(this, ShowMapActivity.class);
                intent.putParcelableArrayListExtra("bus.stops", busStops);
                intent.putParcelableArrayListExtra("bus.route", busRoute);
                intent.putExtra("bus.color", busColor);
                startActivity(intent);
                return true;
        }
        return false;
    }

    private void createCollection(String category, List<? extends Route> list) {

        groupList.add(category);
        categoryCollection.put(category, list);

    }

    private void setRouteItemsVisible(){
        for(RouteItem routeItem : busRoute){
            if(routeItem.getShowInList().equals("true")){
                busRouteVisible.add(routeItem);
            }
        }
    }

    public void initImageFetcher(){
        final int longest = 70;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, MainActivity.IMAGE_CACHE_DIR);
        cacheParams.compressQuality = 100;
        cacheParams.compressFormat = Bitmap.CompressFormat.PNG;
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        mImageFetcher.setLoadingImage(R.drawable.img_avatar_80px);
        mImageFetcher.setImageFadeIn(false);
    }

    public ImageFetcher getImageFetcher(){
        return mImageFetcher;
    }
}
