package com.tenidev.busmap;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tenidev.downloader.IconDownloader;
import com.tenidev.entity.Bus;
import com.tenidev.entity.RequestBus;
import com.tenidev.entity.RouteItem;
import com.tenidev.entity.Stop;
import com.tenidev.map.GMapDirection;
import com.tenidev.network.NetworkUtil;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MapActivity extends FragmentActivity {

    static final LatLng DEFAULT_CENTER = new LatLng(-27.478649, -58.81517);
    private Boolean goBack = false;
    private Boolean actionSearch = false;

    private GoogleMap map;

    Marker markerMyLocation;
    Marker markerMyArrival;
    ArrayList<Polyline> recomendedRouteToStopList = new ArrayList<Polyline>();
    ArrayList<Marker> recomendedStopList = new ArrayList<Marker>();

    Boolean isGPSEnabled = false;
    Boolean isNetworkEnabled = false;

    Boolean detectMyLocation = false;
    Boolean detectMyArrival = false;

    Boolean isAlertNetworkVisible = false;

    LocationManager locManager;
    LocationListener locListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        initMap();

        Bundle bundle = this.getIntent().getExtras();

        Boolean isFindBus = bundle.getBoolean("isFindBus");
        Boolean showMyLocation = bundle.getBoolean("showMyLocation");
        Boolean showMyArrival = bundle.getBoolean("showMyArrival");

        detectMyLocation = bundle.getBoolean("detectMyLocation");
        detectMyArrival = bundle.getBoolean("detectMyArrival");

        initLocationManager();

        if(detectMyLocation){
            checkGpsAndNetworkStatus();

            Location location = getCurrentLocation();
            LatLng position;

            if(location != null)
                position = new LatLng(location.getLatitude(), location.getLongitude());
            else
                position = new LatLng(
                        (Double) bundle.getDouble("markerMyLocation.latitude", DEFAULT_CENTER.latitude),
                        (Double) bundle.getDouble("markerMyLocation.longitude", DEFAULT_CENTER.longitude));

            markerMyLocation = addMarkerCoordinate(markerMyLocation, position, getResources().getString(R.string.marker_my_location_info), R.drawable.ic_my_location);
            markerMyLocation.showInfoWindow();
        }

        if(detectMyArrival){
            checkGpsAndNetworkStatus();

            Location location = getCurrentLocation();
            LatLng position;

            if(location != null)
                position = new LatLng(location.getLatitude(), location.getLongitude());
            else
                position =  new LatLng(
                        (Double) bundle.getDouble("markerMyArrival.latitude", DEFAULT_CENTER.latitude),
                        (Double) bundle.getDouble("markerMyArrival.longitude", DEFAULT_CENTER.longitude));

            markerMyArrival = addMarkerCoordinate(markerMyArrival, position, getResources().getString(R.string.marker_my_arrival_info), R.drawable.ic_my_arrival);
            markerMyArrival.showInfoWindow();
        }

        if(showMyLocation && !detectMyLocation){
            LatLng myLocationPosition = new LatLng(
                    (Double) bundle.getDouble("markerMyLocation.latitude", DEFAULT_CENTER.latitude),
                    (Double) bundle.getDouble("markerMyLocation.longitude", DEFAULT_CENTER.longitude));


            markerMyLocation = addMarkerCoordinate(markerMyLocation, myLocationPosition, getResources().getString(R.string.marker_my_location_info), R.drawable.ic_my_location);
            markerMyLocation.showInfoWindow();
        }

        if(showMyArrival && !detectMyArrival){
            LatLng myArrivalPosition = new LatLng(
                    (Double) bundle.getDouble("markerMyArrival.latitude", DEFAULT_CENTER.latitude),
                    (Double) bundle.getDouble("markerMyArrival.longitude", DEFAULT_CENTER.longitude));

            markerMyArrival = addMarkerCoordinate(markerMyArrival, myArrivalPosition, getResources().getString(R.string.marker_my_arrival_info), R.drawable.ic_my_arrival);
            markerMyArrival.showInfoWindow();
        }


        if(showMyArrival && showMyLocation){
            actionSearch = true;
        }

        if(isFindBus){
            findBus();
            if(MainActivity.MapSearch.dialog != null || MainActivity.MapSearch.dialog.isShowing())
                MainActivity.MapSearch.dialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);

        if( actionSearch && menu.getItem(0) != null ){
            menu.getItem(0).setTitle(getString(R.string.label_find_route));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        return SelectMenu(item);
    }

    private boolean SelectMenu(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_map:
                if(actionSearch){
                    findBus();
                }else {
                    finish();
                }
                return true;
        }
        return false;
    }

    @Override
    public void finish() {

        goBack = true;

        Intent intent = new Intent();

        intent.putExtra("markerMyLocation.latitude", (markerMyLocation == null ? DEFAULT_CENTER.latitude : markerMyLocation.getPosition().latitude));
        intent.putExtra("markerMyLocation.longitude", (markerMyLocation == null ? DEFAULT_CENTER.longitude : markerMyLocation.getPosition().longitude));
        intent.putExtra("markerMyArrival.latitude", (markerMyArrival == null ? DEFAULT_CENTER.latitude : markerMyArrival.getPosition().latitude));
        intent.putExtra("markerMyArrival.longitude", (markerMyArrival == null ? DEFAULT_CENTER.longitude : markerMyArrival.getPosition().longitude));

        intent.putExtra("detectMyLocation", detectMyLocation);
        intent.putExtra("detectMyArrival", detectMyArrival);

        setResult(RESULT_OK,intent);

        /*if(!goBack){
            Intent returnIntent = new Intent();
            setResult(RESULT_CANCELED, returnIntent);
        }*/
        super.finish();
    }

    public void findBus(){

        clearMap();
        getMarkersFromAddress();

        if( NetworkUtil.getConnectivityStatus(getApplication()) == NetworkUtil.TYPE_NOT_CONNECTED ){

            Toast.makeText(getApplicationContext(), getString(R.string.no_network_connection_toast), Toast.LENGTH_LONG).show();
            return;
        }

        Map<RequestBus, Double> requestBusMapMyLocation = new HashMap<RequestBus, Double>();
        Map<RequestBus, Double> requestBusMapMyArrival = new HashMap<RequestBus, Double>();

        if(markerMyLocation != null){
            for(Bus bus : MainActivity.BusList.busArrayList){
                for(Stop stopItem : bus.getStops()){

                    Double distance = distance(markerMyLocation.getPosition(), new LatLng(stopItem.getLatitude(), stopItem.getLongitude()));
                    requestBusMapMyLocation.put( (new RequestBus()).setBus(bus).setDistance(distance).setStop(stopItem), distance );

                }
            }
        }

        RequestBus minDistanceMyLocation = getRequestMin(requestBusMapMyLocation);

        Stop stop = minDistanceMyLocation.getStop();
        LatLng coords = new LatLng(stop.getLatitude(), stop.getLongitude());

        Marker markerStop = addStopMarker(coords, stop.getDescription(), stop.getIcon());

        Polyline polyline = traceStopsRoute(markerMyLocation.getPosition(), coords, GMapDirection.MODE_WALKING, "#0098CC");

        recomendedRouteToStopList.add(polyline);
        recomendedStopList.add(markerStop);


        for(Stop stopItem : minDistanceMyLocation.getBus().getStops()){

            Double distance = distance(markerMyArrival.getPosition(), new LatLng(stopItem.getLatitude(), stopItem.getLongitude()));
            requestBusMapMyArrival.put( (new RequestBus()).setBus(minDistanceMyLocation.getBus()).setDistance(distance).setStop(stopItem), distance );

        }

        RequestBus minDistanceMyArrival = getRequestMin(requestBusMapMyArrival);
        stop = minDistanceMyArrival.getStop();
        coords = new LatLng(stop.getLatitude(), stop.getLongitude());

        markerStop = addStopMarker(coords, stop.getDescription(), stop.getIcon());

        polyline = traceStopsRoute(markerMyArrival.getPosition(), coords, GMapDirection.MODE_WALKING, "#0098CC");

        recomendedRouteToStopList.add(polyline);
        recomendedStopList.add(markerStop);

        polyline = traceBusRoute(minDistanceMyLocation.getBus().getRoute(), minDistanceMyLocation.getBus().getColor());
        recomendedRouteToStopList.add(polyline);

        if (map!=null){
            map.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(markerMyLocation.getPosition(), 16)), 2000, null);
        }
    }

    public void getMarkersFromAddress(){

        try {

            GMapDirection mapDirection = new GMapDirection();

            if( (!MainActivity.MapSearch.editTextMyLocation.getText().toString().trim().equals(getResources().getString(R.string.my_map_point)) &&
                 !MainActivity.MapSearch.editTextMyLocation.getText().toString().trim().equals(getResources().getString(R.string.my_current_location).trim())) &&
                 !MainActivity.MapSearch.editTextMyLocation.getText().toString().isEmpty() ){

                String address = MainActivity.MapSearch.editTextMyLocation.getText().toString().replaceAll("\\s","+");
                LatLng position = mapDirection.getLatLngFromAddress(address);

                if(position != null){
                    addMarkerCoordinate(markerMyLocation, position, getResources().getString(R.string.marker_my_location_info), R.drawable.ic_my_location).showInfoWindow();

                    if (map!=null){
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                        map.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
                    }
                }
            }

            if( !MainActivity.MapSearch.editTextMyArrival.getText().toString().equals(getResources().getString(R.string.my_current_location)) &&
                !MainActivity.MapSearch.editTextMyArrival.getText().toString().equals(getResources().getString(R.string.my_map_point)) &&
                !MainActivity.MapSearch.editTextMyArrival.getText().toString().isEmpty()){

                String address = MainActivity.MapSearch.editTextMyArrival.getText().toString().replaceAll("\\s","+");
                LatLng position = mapDirection.getLatLngFromAddress(address);

                if(position != null){
                    addMarkerCoordinate(markerMyArrival, position, getResources().getString(R.string.marker_my_arrival_info), R.drawable.ic_my_arrival).showInfoWindow();

                    if (map!=null){
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                        map.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
                    }
                }
            }

        }catch (Exception e){
            return;
        }
    }

    private Marker addStopMarker(LatLng position, String description, String urlIcon){

        MarkerOptions markerOptions = new MarkerOptions().position(position)
                .title(description);

        if(urlIcon.isEmpty()){

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stop_standard));
            return map.addMarker(markerOptions);

        }else{
            //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(imageLoader.getBitmap(urlIcon, R.drawable.ic_stop_standard)));
            IconDownloader iconDownloader = new IconDownloader(map, markerOptions);
            iconDownloader.execute(urlIcon);

            return iconDownloader.marker;
        }
    }

    private void initMap(){
        if (map == null){
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFindRoute)).getMap();
        }

        final View view = getWindow().getDecorView().getRootView();

        // Move the camera instantly to hamburg with a zoom of 15.
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_CENTER, 15));
        // Zoom in, animating the camera.
        map.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                // TODO Auto-generated method stub

                if(markerMyLocation != null){
                    if(marker.getPosition().equals(markerMyLocation.getPosition())){
                        MainActivity.MapSearch.setLeyendMapPoint(MainActivity.MapSearch.EditText.MY_LOCATION, view);
                        detectMyLocation = false;
                    }
                }else if(markerMyArrival != null){
                    if(marker.getPosition().equals(markerMyArrival.getPosition())){
                        MainActivity.MapSearch.setLeyendMapPoint(MainActivity.MapSearch.EditText.MY_ARRIVAL, view);
                        detectMyArrival = false;
                    }
                }
                clearMap();
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                // TODO Auto-generated method stub
            }

        });
    }

    public void clearMap(){
        // Elimnamos los marcadores
        for (Marker marker : recomendedStopList){
            if(marker != null)
                marker.remove();
        }
        // Eliminamos las rutas
        for(Polyline polyline : recomendedRouteToStopList){
            if(polyline != null)
                polyline.remove();
        }
    }


    public Marker addMarkerCoordinate(Marker marker, LatLng position, String title, int icon){
        if(marker != null){

            marker.setPosition(position);

            if (map!=null){
                map.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(position, 16)), 2000, null);
            }

        }else{

            marker = map.addMarker(new MarkerOptions()
                    .position(position)
                    .title(title)
                    .icon(BitmapDescriptorFactory.fromResource(icon))
                    .draggable(true)
            );

            if (map!=null){
                map.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(position, 16)), 2000, null);
            }

        }

        return  marker;
    }

    public static double distance(LatLng StartP, LatLng EndP) {
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return 6366000 * c;
    }

    public Polyline traceStopsRoute(LatLng from, LatLng to, String mode, String color){

        GMapDirection mapDirection = new GMapDirection();

        Document doc = mapDirection.getDocument(from, to, mode);
        ArrayList<LatLng> directionPoint = mapDirection.getDirection(doc);
        PolylineOptions rectLine = new PolylineOptions().color(Color.parseColor(color));

        for(int i = 0 ; i < directionPoint.size() ; i++) {
            rectLine.add(directionPoint.get(i));
        }
        return map.addPolyline(rectLine);
    }

    public Polyline traceBusRoute(ArrayList<RouteItem> busRoute, String busColor){

        PolylineOptions rectOptions = new PolylineOptions();
        rectOptions.color(Color.parseColor(busColor));

        for(RouteItem routeItem: busRoute){

            LatLng coords = new LatLng(routeItem.getLatitude(), routeItem.getLongitude());

            rectOptions.add(coords);

        }
        // Get back the mutable Polyline
        return map.addPolyline(rectOptions);
    }

    private RequestBus getRequestMin(Map<RequestBus, Double> request){

        Map.Entry<RequestBus, Double> minDistance = Collections.min(request.entrySet(), new Comparator<Map.Entry<RequestBus, Double>>() {
            public int compare(Map.Entry<RequestBus, Double> entry1, Map.Entry<RequestBus, Double> entry2) {
                return entry1.getValue().compareTo(entry2.getValue());
            }
        });

        return minDistance.getKey();
    }

    public Location getCurrentLocation() {

        Location location = null;

        try {
            //checkGpsAndNetworkStatus();

            if (!isGPSEnabled && !isNetworkEnabled) {

            } else {

                if (isNetworkEnabled) {
                    locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 90000, 0, locListener);

                    if (locManager != null) {
                        location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        //location = map.getMyLocation();
                        if (location != null) {
                            if(addGpsMarker(location) != null){
                                locManager.removeUpdates(locListener);
                            }
                        }else{
                            locManager.removeUpdates(locListener);
                            Toast.makeText(getApplicationContext(), "Momentaneamente no hemos podido obtener tu ubicacion.", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 90000, 0, locListener);

                        if (locManager != null) {
                            location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            //location = map.getMyLocation();
                            if (location != null) {
                                if(addGpsMarker(location) != null){
                                    locManager.removeUpdates(locListener);
                                }
                            }else{
                                locManager.removeUpdates(locListener);
                                Toast.makeText(getApplicationContext(), "Momentaneamente no hemos podido obtener tu ubicacion.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    private Boolean checkGpsAndNetworkStatus(){

        isGPSEnabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(!isGPSEnabled && !isNetworkEnabled){
            showAlertNetworkLocationDisabled();
            return false;
        }

        if (!isGPSEnabled) {
            showAlertGpsDisabled();
            return false;
        }

        return true;
    }

    private Location addGpsMarker(Location location){
        try{
            if(location == null){
                Toast.makeText(getBaseContext(), "No se puede localizar tu ubicacion.", Toast.LENGTH_SHORT).show();
            }
            else if(detectMyLocation){

                LatLng position = new LatLng(location.getLatitude(), location.getLongitude());

                addMarkerCoordinate(markerMyLocation, position, getResources().getString(R.string.marker_my_location_info), R.drawable.ic_my_location).showInfoWindow();

            }else if(detectMyArrival){

                LatLng position = new LatLng(location.getLatitude(), location.getLongitude());

                addMarkerCoordinate(markerMyArrival, position, getResources().getString(R.string.marker_my_arrival_info), R.drawable.ic_my_arrival).showInfoWindow();
            }
        }
        catch (Exception e){
            return null;
        }

        return location;
    }

    private void showAlertGpsDisabled(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapActivity.this);
        alertDialog.setTitle("Mejora tu ubicacion");
        alertDialog.setMessage("Activa las siguientes funciones para mejorar tu ubicacion:\r\n > GPS");
        //alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("HABILITAR",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isAlertNetworkVisible = true;
                        Intent act = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(act);
                    }
                });

        alertDialog.setNegativeButton("CANCELAR",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog.show();
    }

    private void showAlertNetworkLocationDisabled(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapActivity.this);
        alertDialog.setTitle("Mejora tu ubicacion");
        alertDialog.setMessage("Lamentablemente no podemos acceder a tu ubicacion.\r\nActiva las siguientes opciones:\r\n > Acceso a mi ubicacion\r\n > Ubicacion de red Wi-Fi y movil.\r\n > GPS.");
        //alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("HABILITAR",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isAlertNetworkVisible = true;
                        Intent act = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(act);
                    }
                });

        alertDialog.setNegativeButton("CANCELAR",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog.show();
    }

    private void initLocationManager(){

        locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        locListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                //addGpsMarker(location);
            }

            public void onProviderDisabled(String provider){

            }

            public void onProviderEnabled(String provider){

            }

            public void onStatusChanged(String provider, int status, Bundle extras){

            }
        };

        isGPSEnabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isAlertNetworkVisible){

            if(detectMyLocation){
                Location location = getCurrentLocation();
                LatLng position;

                if(location != null)
                    position = new LatLng(location.getLatitude(), location.getLongitude());
                else
                    position = new LatLng(
                            DEFAULT_CENTER.latitude,
                            DEFAULT_CENTER.longitude);

                markerMyLocation = addMarkerCoordinate(markerMyLocation, position, getResources().getString(R.string.marker_my_location_info), R.drawable.ic_my_location);
                markerMyLocation.showInfoWindow();
            }

            if(detectMyArrival){

                Location location = getCurrentLocation();
                LatLng position;

                if(location != null)
                    position = new LatLng(location.getLatitude(), location.getLongitude());
                else
                    position =  new LatLng(
                            DEFAULT_CENTER.latitude,
                            DEFAULT_CENTER.longitude);

                markerMyArrival = addMarkerCoordinate(markerMyArrival, position, getResources().getString(R.string.marker_my_arrival_info), R.drawable.ic_my_arrival);
                markerMyArrival.showInfoWindow();
            }

            isAlertNetworkVisible = false;
        }
    }
}
