package com.tenidev.busmap;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.tenidev.adapter.DefaultRowAdapter;
import com.tenidev.downloader.util.ImageCache;
import com.tenidev.downloader.util.ImageFetcher;
import com.tenidev.entity.Bus;
import com.tenidev.network.NetworkUtil;
import com.tenidev.xml.LoadBusXml;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    public static final String IMAGE_CACHE_DIR = "images";
    private ImageFetcher mImageFetcher;

    static final LatLng MAP_DEFAULT_CENTER = new LatLng(-27.478649, -58.81517);
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    private ActionBar currentActionBar = null;
    private Menu currentMenu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initImageFetcher();

        checkInternet(this);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        currentActionBar = actionBar;

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            //.setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setIcon(mSectionsPagerAdapter.getIcon(i))
                            .setTabListener(this));
        }

        handleIntent(getIntent());
    }

    public static Boolean checkInternet(Context context){
        if( NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED ){
            Toast.makeText(context, context.getString(R.string.no_network_connection_toast), Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
             doSearch(query);
        }
    }

    public void doSearch(String query){
        BusList.adapterBusList.getFilter().filter(query);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        currentMenu = menu;
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        return SelectMenu(item);
    }

    private boolean SelectMenu(MenuItem item){
        switch (item.getItemId()){

            case android.R.id.home:
                return true;

            case R.id.action_search:
                // Get the SearchView and set the searchable configuration
                SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
                final SearchView searchView = (SearchView) item.getActionView();
                // Assumes current activity is the searchable activity
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
                searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

                SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener()
                {
                    public boolean onQueryTextChange(String newText)
                    {
                        // this is your adapter that will be filtered
                        doSearch(newText);
                        return true;
                    }

                    public boolean onQueryTextSubmit(String query)
                    {
                        // this is your adapter that will be filtered
                        doSearch(query);
                        hideKeysboard( searchView.getRootView() );
                        return true;
                    }
                };
                searchView.setOnQueryTextListener(queryTextListener);
                return true;

            case R.id.action_update:
                MainActivity.BusList.updateList();
                return true;

            case R.id.action_about:
                showAbout(this);
                return true;

        }
        return false;
    }

    /*@Override
    public boolean onSearchRequested() {
        //pauseSomeStuff();
        return super.onSearchRequested();
    }*/

    private void showAbout(Context context){
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_about);
        dialog.setTitle("Acerca de...");

        LinearLayout layoutAbout = (LinearLayout) dialog.findViewById(R.id.layoutAbout);
        layoutAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void hideKeysboard(View view){
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());

        if(currentMenu == null)
            return;

        MenuItem menuItemSearch = currentMenu.getItem(0);
        MenuItem menuItemUpdate = currentMenu.getItem(1);
        if(tab.getPosition() == 1)
        {
            if(menuItemSearch != null)
                menuItemSearch.setVisible(false);

            if(menuItemUpdate != null)
                menuItemUpdate.setVisible(false);
        }
        else if(tab.getPosition() != 1)
        {
            if(menuItemSearch != null && !menuItemSearch.isVisible())
                menuItemSearch.setVisible(true);

            if(menuItemUpdate != null && !menuItemUpdate.isVisible())
                menuItemUpdate.setVisible(true);
        }

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = new Fragment();

            switch (position){

                case 0:
                    fragment = new BusList();
                    break;

                case 1:
                    fragment = new MapSearch();
                    break;

            }

            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
            }
            return null;
        }

        public Drawable getIcon(int position){
            switch (position) {
                case 0:
                    return getResources().getDrawable(R.drawable.ic_icon_list_04_03);
                case 1:
                    return getResources().getDrawable(R.drawable.ic_icon_gps_05_04);
            }
            return null;
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply
     * displays dummy text.
     */
    public static class BusList extends Fragment {

        public static ArrayList<Bus> busArrayList = new ArrayList<Bus>();
        static final String mURL = "http://www.tenidev.com.ar/busmap.xml";
        public static DefaultRowAdapter adapterBusList;
        private static ListView listView;
        public static View rootView;
        public static LayoutInflater layoutInflater;

        public BusList() { }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            rootView = inflater.inflate(R.layout.main_bus_list, container, false);
            layoutInflater = inflater;

            loadList();

            return rootView;
        }

        private void loadList() {

            try{

                busArrayList = new LoadBusXml(this.getActivity()).execute(mURL).get();

            }
            catch (InterruptedException e) {        }
            catch (ExecutionException e) {        }

            View header = (View) layoutInflater.inflate(R.layout.header_bus_list, null);
            adapterBusList = new DefaultRowAdapter(this.getActivity(), busArrayList);

            listView = (ListView) rootView.findViewById(R.id.listView);
            listView.addHeaderView(header, null, false);
            listView.setAdapter(adapterBusList);

            listView.setTextFilterEnabled(true);

            listView.setOnItemClickListener(new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                    Bus bus = (Bus) a.getItemAtPosition(position);
                    Intent intent = new Intent(v.getContext(), BusDetailActivity.class);
                    intent.putExtra("bus.title", bus.getTitle() );
                    intent.putExtra("bus.neighborhood", bus.getNeighborhood());
                    intent.putExtra("bus.thumb", bus.getThumb());
                    intent.putExtra("bus.distanceTraveled", bus.getDistanceTraveled());
                    intent.putExtra("bus.journeyTime", bus.getJourneyTime());
                    intent.putParcelableArrayListExtra("bus.stops", bus.getStops());
                    intent.putParcelableArrayListExtra("bus.route", bus.getRoute());
                    intent.putExtra("bus.color", bus.getColor());
                    startActivity(intent);
                }
            });
        }

        public static void updateList() {

            if(!checkInternet(rootView.getContext()))
                return;

            try{

                busArrayList = new LoadBusXml(rootView.getContext()).execute(mURL).get();

            }
            catch (InterruptedException e) {        }
            catch (ExecutionException e) {        }

            View header = (View) layoutInflater.inflate(R.layout.header_bus_list, null);
            adapterBusList = new DefaultRowAdapter(rootView.getContext(), busArrayList);

            listView = (ListView) rootView.findViewById(R.id.listView);
            listView.setAdapter(adapterBusList);

            listView.setTextFilterEnabled(true);

            listView.setOnItemClickListener(new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                    Bus bus = (Bus) a.getItemAtPosition(position);
                    Intent intent = new Intent(v.getContext(), BusDetailActivity.class);
                    intent.putExtra("bus.title", bus.getTitle() );
                    intent.putExtra("bus.neighborhood", bus.getNeighborhood());
                    intent.putExtra("bus.thumb", bus.getThumb());
                    intent.putExtra("bus.distanceTraveled", bus.getDistanceTraveled());
                    intent.putExtra("bus.journeyTime", bus.getJourneyTime());
                    intent.putParcelableArrayListExtra("bus.stops", bus.getStops());
                    intent.putParcelableArrayListExtra("bus.route", bus.getRoute());
                    intent.putExtra("bus.color", bus.getColor());
                    rootView.getContext().startActivity(intent);
                }
            });
        }
    }


    public static class MapSearch extends Fragment {

        public static enum EditText{
            MY_LOCATION,
            MY_ARRIVAL
        };

        Boolean detectMyLocation = false;
        Boolean detectMyArrival = false;

        public static AlertDialog dialog;
        public static android.widget.EditText editTextMyArrival;
        public static android.widget.EditText editTextMyLocation;
        private Button buttonMyLocationOptions;
        private Button buttonMyArrivalOptions;
        private Button buttonFindRoute;

        private LatLng markerMyLocation;
        private LatLng markerMyArrival;

        final CharSequence[] itemsDialog = {"Mi ubicacion actual", "Punto en el mapa"};

        public MapSearch() { }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.main_map_search_form, container, false);

            initEditTexts(rootView);
            initButtonOptions(rootView);
            initButtonFindRoute(rootView);

            return rootView;
        }

        private void initEditTexts(View rootView){

            editTextMyArrival = (android.widget.EditText) rootView.findViewById(R.id.editTextMyArrival);
            editTextMyLocation = (android.widget.EditText) rootView.findViewById(R.id.editTextMyLocation);

            editTextMyLocation.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    editTextMyLocation.setTextColor(Color.BLACK);
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().compareTo(getView().getResources().getString(R.string.my_current_location)) > 0){
                        detectMyArrival = false;
                    }
                }
            });

            editTextMyArrival.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    editTextMyArrival.setTextColor(Color.BLACK);
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().compareTo(getView().getResources().getString(R.string.my_current_location)) > 0){
                        detectMyLocation = false;
                    }
                }
            });

            editTextMyArrival.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(!b) {
                        InputMethodManager imm =  (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            });

            editTextMyLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(!b) {
                        InputMethodManager imm =  (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            });
        }

        private void initButtonOptions(View rootView){

            buttonMyLocationOptions = (Button) rootView.findViewById(R.id.buttonMyLocationOptions);
            buttonMyLocationOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle(getResources().getString(R.string.select_my_location_point));
                    builder.setItems(itemsDialog, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            switch (item) {
                                case 0:
                                    detectMyLocation = true;
                                    if(hasErrors()){
                                        detectMyLocation = false;
                                        return;
                                    }
                                    setLeyendCurrentLocation(EditText.MY_LOCATION, getView());
                                    showMap(getView(), false, true, false);
                                    break;
                                case 1:
                                    detectMyLocation = false;
                                    setLeyendMapPoint(EditText.MY_LOCATION, getView());
                                    showMap(getView(), false, true, false);
                                    break;
                            }
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            buttonMyArrivalOptions = (Button) rootView.findViewById(R.id.buttonMyArrivalOptions);
            buttonMyArrivalOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle("Seleccionar punto de destino");
                    builder.setItems(itemsDialog, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            switch (item){
                                case 0:
                                    detectMyArrival = true;
                                    if (hasErrors()){
                                        detectMyArrival = false;
                                        return;
                                    }
                                    setLeyendCurrentLocation(EditText.MY_ARRIVAL, getView());
                                    showMap(getView(), false, false, true);
                                    break;
                                case 1:
                                    detectMyArrival = false;
                                    setLeyendMapPoint(EditText.MY_ARRIVAL, getView());
                                    showMap(getView(), false, false, true);
                                    break;
                            }
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
        }

        public Boolean hasErrors(){
            if(detectMyLocation && detectMyArrival){
                Toast.makeText(getView().getContext(), getString(R.string.error1), Toast.LENGTH_LONG).show();
                return true;
            }
            if( editTextMyArrival.getText().toString().trim().toLowerCase().compareTo( editTextMyLocation.getText().toString().trim().toLowerCase() ) > 0 ){
                return true;
            }
            return false;
        }

        public void initButtonFindRoute(View view){
            buttonFindRoute = (Button) view.findViewById(R.id.buttonFindRoute);
            buttonFindRoute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editTextMyLocation.clearFocus();
                    editTextMyArrival.clearFocus();

                    if(editTextMyArrival.getText().toString().isEmpty() ||
                       editTextMyLocation.getText().toString().isEmpty()){

                        Toast.makeText(view.getContext(), getString(R.string.error_one_field_is_empty), Toast.LENGTH_LONG).show();
                        return;
                    }

                    dialog = new ProgressDialog(view.getContext());
                    dialog.setMessage(view.getResources().getString(R.string.processing_route));
                    dialog.show();

                    showMap(view, true, true, true);
                }
            });
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (requestCode == 1) {

                if(resultCode == RESULT_OK){
                    Double myLocationLatitude =  data.getDoubleExtra("markerMyLocation.latitude", MAP_DEFAULT_CENTER.latitude);
                    Double myLocationLongitude =  data.getDoubleExtra("markerMyLocation.longitude", MAP_DEFAULT_CENTER.longitude);
                    Double myArrivalLatitude = data.getDoubleExtra("markerMyArrival.latitude", MAP_DEFAULT_CENTER.latitude);
                    Double myArrivalLongitude = data.getDoubleExtra("markerMyArrival.longitude", MAP_DEFAULT_CENTER.longitude);

                    detectMyLocation = data.getBooleanExtra("detectMyLocation", false);
                    detectMyArrival = data.getBooleanExtra("detectMyArrival", false);

                    markerMyLocation = new LatLng(myLocationLatitude, myLocationLongitude);
                    markerMyArrival = new LatLng(myArrivalLatitude, myArrivalLongitude);
                }
                if (resultCode == RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

        public void showMap(View view, Boolean isFindBus, Boolean selectMyLocation, Boolean selectMyArrival){
            Intent intent = new Intent(view.getContext(), MapActivity.class);

            intent.putExtra("isFindBus", false);

            intent.putExtra("detectMyLocation", false);
            intent.putExtra("detectMyArrival", false);

            intent.putExtra("showMyLocation", true);
            intent.putExtra("showMyArrival", true);

            if(editTextMyArrival.getText().toString().isEmpty()){
                intent.putExtra("showMyArrival", false);
            }

            if(editTextMyLocation.getText().toString().isEmpty()){
                intent.putExtra("showMyLocation", false);
            }

            if(selectMyLocation){
                intent.putExtra("selectMyLocation", true);
            }

            if(selectMyArrival){
                intent.putExtra("selectMyArrival", true);
            }

            if(detectMyLocation){
                intent.putExtra("detectMyLocation", true);
            }

            if(detectMyArrival){
                intent.putExtra("detectMyArrival", true);
            }

            if(isFindBus){
                //intent.putParcelableArrayListExtra("bus.list", busArrayList);
                intent.putExtra("isFindBus", true);
            }

            intent.putExtra("markerMyLocation.latitude", (markerMyLocation == null ? MAP_DEFAULT_CENTER.latitude : markerMyLocation.latitude));
            intent.putExtra("markerMyLocation.longitude", (markerMyLocation == null ? MAP_DEFAULT_CENTER.longitude : markerMyLocation.longitude));

            intent.putExtra("markerMyArrival.latitude", (markerMyArrival == null ? MAP_DEFAULT_CENTER.latitude : markerMyArrival.latitude));
            intent.putExtra("markerMyArrival.longitude", (markerMyArrival == null ? MAP_DEFAULT_CENTER.longitude : markerMyArrival.longitude));

            startActivityForResult(intent, 1);
        }

        public static void setLeyendCurrentLocation(EditText editTextType, View view){
            android.widget.EditText editText = new android.widget.EditText(view.getContext());

            if(editTextType == EditText.MY_LOCATION){
                editText = editTextMyLocation;
            }
            if(editTextType == EditText.MY_ARRIVAL){
                editText = editTextMyArrival;
            }

            editText.setText(view.getResources().getString(R.string.my_current_location));
            editText.setTextColor( view.getResources().getColor(R.color.translucent_cyan));
            editText.clearFocus();
        }

        public static void setLeyendMapPoint(EditText editTextType, View view){
            android.widget.EditText editText = new android.widget.EditText(view.getContext());

            if(editTextType == EditText.MY_LOCATION){
                editText = editTextMyLocation;
            }
            if(editTextType == EditText.MY_ARRIVAL){
                editText = editTextMyArrival;
            }

            editText.setText(view.getResources().getString(R.string.my_map_point));
            editText.setTextColor( view.getResources().getColor(R.color.translucent_cyan));
            editText.clearFocus();
        }

    }


    public void initImageFetcher(){
        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.compressQuality = 100;
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory
        cacheParams.compressFormat = Bitmap.CompressFormat.PNG;

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(false);
        mImageFetcher.setLoadingImage(R.drawable.img_avatar_80px);
    }

    public ImageFetcher getImageFetcher(){
        return mImageFetcher;
    }

}
