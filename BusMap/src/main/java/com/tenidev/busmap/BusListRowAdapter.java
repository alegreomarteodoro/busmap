package com.tenidev.busmap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tenidev.entity.Bus;
import com.tenidev.web.ImageLoader;

import java.util.ArrayList;

/**
 * Created by alegreomarteodoro on 19/07/13.
 */
public class BusListRowAdapter extends BaseAdapter {

    private Activity activity;
    private static LayoutInflater inflater=null;
    private ArrayList<Bus> data;
    public ImageLoader imageLoader;

    public BusListRowAdapter(Activity a, ArrayList l){
        activity = a;
        data = l;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public Object getItemAtPosition(int position){
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public int getCount() {
        return data.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.layout_list_row, null);

        TextView title = (TextView)vi.findViewById(R.id.option_text); // title
        ImageView thumb_image = (ImageView)vi.findViewById(R.id.option_icon); // thumb image

        Bus currentBus = (Bus) data.get(position);

        title.setText(currentBus.getTitle() + " - " + currentBus.getNeighborhood()); //set text
        imageLoader.DisplayImage(currentBus.getIcon(), thumb_image);
        /*if (thumb_image != null) {
            new ImageDownloader(thumb_image).execute(currentBus.getIcon());
        }*/
        return vi;
    }

}
