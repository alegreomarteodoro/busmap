package com.tenidev.busmap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tenidev.entity.Route;
import com.tenidev.entity.RouteItem;
import com.tenidev.entity.Stop;
import com.tenidev.web.ImageLoader;

import java.util.List;
import java.util.Map;

/**
 * Created by alegreomarteodoro on 26/07/13.
 */
public class DetailExpandableListAdapter extends BaseExpandableListAdapter {
    private Activity context;
    private Map<String, List<? extends Route>> categoryCollection;
    private List<String> groupList;
    private ImageLoader imageLoader;

    public DetailExpandableListAdapter(Activity context, List<String> groupList,
                                       Map<String, List<? extends Route>> categoryCollection) {
        this.context = context;
        this.categoryCollection = categoryCollection;
        this.groupList = groupList;
        this.imageLoader = new ImageLoader(context.getApplicationContext());
    }

    public Route getChild(int groupPosition, int childPosition) {
        return categoryCollection.get(groupList.get(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_list_row, null);
        }

        RouteItem routeItem;
        Stop stop;

        if (getChild(groupPosition, childPosition) instanceof RouteItem)
        {

            routeItem = (RouteItem) getChild(groupPosition, childPosition);

            ImageView imageView = (ImageView) convertView.findViewById(R.id.option_icon);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageLoader.DisplayImage(routeItem.getIcon(), imageView, R.drawable.ic_launcher);

            TextView textView = (TextView) convertView.findViewById(R.id.option_text);
            textView.setText(routeItem.getDescription());

        }else if(getChild(groupPosition, childPosition) instanceof Stop){

            stop = (Stop) getChild(groupPosition, childPosition);

            ImageView imageView = (ImageView) convertView.findViewById(R.id.option_icon);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageLoader.DisplayImage(stop.getIcon(), imageView, R.drawable.ic_stop_standard);

            TextView textView = (TextView) convertView.findViewById(R.id.option_text);
            textView.setText(stop.getDescription());

        }

        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return categoryCollection.get(groupList.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    public int getGroupCount() {
        return groupList.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        String categoryName = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_header_expandible_list, null);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.header_stoplist);
        //textView.setTypeface(null, Typeface.BOLD);
        textView.setText(categoryName);

        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
