package com.tenidev.busmap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by alegreomarteodoro on 11/08/13.
 */
public class TooltipScreen extends Activity {

    static int currentTooltip = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tooltip);

        currentTooltip++;

        final LinearLayout layout = (LinearLayout) findViewById(R.id.tooltipBackground);

        Button buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentTooltip == 1){
                     layout.setBackgroundResource(R.drawable.tooltip2);
                     currentTooltip++;
                }
                else if(currentTooltip == 2){
                    layout.setBackgroundResource(R.drawable.tooltip3);
                    currentTooltip++;
                }
                else if(currentTooltip == 3){
                    Intent i = new Intent(TooltipScreen.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        Button buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TooltipScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }
}
