package com.tenidev.entity;

/**
 * Created by alegreomarteodoro on 25/07/13.
 */
public interface Route{

    String icon = null;
    String description = null;
    Float latitude = null;
    Float longitude = null;

    public String getIcon();

    public void setIcon(String icon);

    public String getDescription();

    public void setDescription(String description);

    public Float getLatitude();

    public void setLatitude(Float latitude);

    public Float getLongitude();

    public void setLongitude(Float longitude);
}
