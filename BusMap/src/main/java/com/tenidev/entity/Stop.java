package com.tenidev.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alegreomarteodoro on 24/07/13.
 */
public class Stop implements Parcelable, Route {

    private String icon;
    private String description;
    private Float latitude;
    private Float longitude;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(icon);
        out.writeString(description);
        out.writeFloat(latitude);
        out.writeFloat(longitude);
    }

    public static final Creator<Stop> CREATOR
            = new Creator<Stop>() {
        public Stop createFromParcel(Parcel in) {
            return new Stop(in);
        }

        public Stop[] newArray(int size) {
            return new Stop[size];
        }
    };

    private Stop(Parcel in) {
        icon = in.readString();
        description = in.readString();
        latitude = in.readFloat();
        longitude = in.readFloat();
    }

    public Stop(){

    }
}
