package com.tenidev.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by alegreomarteodoro on 20/07/13.
 */
public class Bus implements Parcelable{

    private Integer id;
    private String title;
    private String neighborhood;
    private String icon;
    private String thumb;
    private String image;
    private String distanceTraveled;
    private String journeyTime;
    private ArrayList<Stop> stops = new ArrayList<Stop>();
    private ArrayList<RouteItem> route = new ArrayList<RouteItem>();
    private String color;

    public ArrayList<RouteItem> getRoute() {
        return route;
    }

    public void addRouteItem(RouteItem route) {
        this.route.add(route);
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDistanceTraveled() {
        return distanceTraveled;
    }

    public void setDistanceTraveled(String distanceTraveled) {
        this.distanceTraveled = distanceTraveled;
    }

    public String getJourneyTime() {
        return journeyTime;
    }

    public void setJourneyTime(String journeyTime) {
        this.journeyTime = journeyTime;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public void addStop(Stop item){
        this.stops.add(item);
    }

    public ArrayList<Stop> getStops(){
        return this.stops;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(icon);
        parcel.writeString(title);
        parcel.writeString(neighborhood);
        parcel.writeString(thumb);
        parcel.writeString(image);
        parcel.writeString(distanceTraveled);
        parcel.writeString(journeyTime);
        parcel.writeString(color);
        parcel.writeList(stops);
        parcel.writeList(route);
    }

    public static final Creator<Bus> CREATOR
            = new Creator<Bus>() {
        public Bus createFromParcel(Parcel in) {
            return new Bus(in);
        }

        public Bus[] newArray(int size) {
            return new Bus[size];
        }
    };

    private Bus(Parcel in) {
        icon = in.readString();
        title = in.readString();
        neighborhood = in.readString();
        thumb = in.readString();
        image = in.readString();
        distanceTraveled = in.readString();
        journeyTime = in.readString();
        color = in.readString();
        in.readTypedList(stops, Stop.CREATOR);
        in.readTypedList(route, RouteItem.CREATOR);
    }

    public Bus(){

    }
}
