package com.tenidev.entity;

/**
 * Created by alegreomarteodoro on 06/08/13.
 */
public class RequestBus {
    Double distance;
    Stop stop;
    Bus bus;

    public Double getDistance() {
        return distance;
    }

    public RequestBus setDistance(Double distance) {
        this.distance = distance;
        return this;
    }

    public Stop getStop() {
        return stop;
    }

    public RequestBus setStop(Stop stop) {
        this.stop = stop;
        return this;
    }

    public Bus getBus() {
        return bus;
    }

    public RequestBus setBus(Bus bus) {
        this.bus = bus;
        return this;
    }
}
