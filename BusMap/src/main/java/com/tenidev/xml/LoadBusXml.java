package com.tenidev.xml;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tenidev.busmap.R;
import com.tenidev.entity.Bus;
import com.tenidev.entity.RouteItem;
import com.tenidev.entity.Stop;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

/**
 * Created by alegreomarteodoro on 20/07/13.
 */
public class LoadBusXml extends AsyncTask<String, String, ArrayList<Bus>> {

    private final ProgressDialog mDialog;
    private Context activity;

    static final String KEY_BUS = "bus"; // parent node
    static final String KEY_ID = "id";
    static final String KEY_TITLE = "title";
    static final String KEY_NEYGHBORHOOD = "neighborhood";
    static final String KEY_ICON = "icon";
    static final String KEY_THUMB = "thumb";
    static final String KEY_DISTANCE_TRAVELED = "distance_traveled";
    static final String KEY_JOURNEY_TIME = "journey_time";
    static final String KEY_IMAGE = "image";
    static final String KEY_STOP = "stop";
    static final String KEY_ROUTEITEM = "route_item";
    static final String KEY_DESCRIPTION = "description";
    static final String KEY_LATITUDE = "latitude";
    static final String KEY_LONGITUDE = "longitude";
    static final String KEY_COLOR = "color";
    static final String KEY_IN_LIST = "show_in_list";


    private ArrayList<Bus> mFeedItems = new ArrayList<Bus>();

    public LoadBusXml(Context activity) {

        mDialog = new ProgressDialog(activity);
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

        mDialog.setMessage(activity.getString(R.string.download_info));
        mDialog.show();
    }

    @Override
    protected ArrayList<Bus> doInBackground(String... feedUrl) {

        try {
            
            ArrayList<Bus> busList = new ArrayList<Bus>();

            XMLParser parser = new XMLParser();
            String xml = parser.getXmlFromUrl(feedUrl[0]); // getting XML from URL
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_BUS);
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                // creating new Bus
                Bus bus = new Bus();

                Element e = (Element) nl.item(i);
                bus.setTitle(parser.getValue(e, KEY_TITLE));
                bus.setNeighborhood(parser.getValue(e, KEY_NEYGHBORHOOD));
                bus.setIcon(parser.getValue(e, KEY_ICON));
                bus.setDistanceTraveled(parser.getValue(e, KEY_DISTANCE_TRAVELED));
                bus.setThumb(parser.getValue(e, KEY_THUMB));
                bus.setJourneyTime(parser.getValue(e, KEY_JOURNEY_TIME));
                bus.setImage(parser.getValue(e, KEY_IMAGE));
                bus.setColor(parser.getValue(e, KEY_COLOR));

                NodeList sl = e.getElementsByTagName(KEY_STOP);
                for (int j = 0; j < sl.getLength(); j++) {

                    Element se = (Element) sl.item(j);

                    Stop stop = new Stop();
                    stop.setIcon(parser.getValue(se, KEY_ICON));
                    stop.setDescription(parser.getValue(se, KEY_DESCRIPTION));
                    stop.setLatitude(Float.parseFloat(parser.getValue(se, KEY_LATITUDE)));
                    stop.setLongitude(Float.parseFloat(parser.getValue(se, KEY_LONGITUDE)));

                    bus.addStop(stop);
                }

                NodeList rl = e.getElementsByTagName(KEY_ROUTEITEM);
                for (int j = 0; j < rl.getLength(); j++) {

                    Element re = (Element) rl.item(j);

                    RouteItem routeItem = new RouteItem();
                    routeItem.setIcon(parser.getValue(re, KEY_ICON));
                    routeItem.setDescription(parser.getValue(re, KEY_DESCRIPTION));
                    routeItem.setLatitude(Float.parseFloat(parser.getValue(re, KEY_LATITUDE)));
                    routeItem.setLongitude(Float.parseFloat(parser.getValue(re, KEY_LONGITUDE)));
                    routeItem.setShowInList(parser.getValue(re, KEY_IN_LIST));

                    bus.addRouteItem(routeItem);
                }

                // adding Bus to ArrayList
                mFeedItems.add(bus);
            }
            

        } catch (Exception e) {

            Log.e("Borked", "feed has borked " + e.toString());

        }

        return mFeedItems;

    }

    @Override
    protected void onPostExecute(ArrayList<Bus> posts) {

        super.onPostExecute(posts);

        mDialog.dismiss();

    }
}