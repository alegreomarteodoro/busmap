package com.tenidev.xml;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;

/**
 * Created by alegreomarteodoro on 03/08/13.
 */
public class LoadXml extends AsyncTask<String, String, Document> {

    Document doc;

    public LoadXml(Activity activity) {    }

    public LoadXml() {    }

    @Override
    protected void onPreExecute() {
            super.onPreExecute();
    }

    @Override
    protected Document doInBackground(String... feedUrl) {

        try {

            XMLParser parser = new XMLParser();
            String xml = parser.getXmlFromUrl(feedUrl[0]); // getting XML from URL
            doc = parser.getDomElement(xml); // getting DOM element

        } catch (Exception e) {
            Log.e("Borked", "feed has borked " + e.toString());
        }
        return doc;
    }

    @Override
    protected void onPostExecute(Document document) {
            super.onPostExecute(document);
    }
}