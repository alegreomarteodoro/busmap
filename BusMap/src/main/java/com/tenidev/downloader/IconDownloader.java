package com.tenidev.downloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tenidev.busmap.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * Created by alegreomarteodoro on 22/07/13.
 */
public class IconDownloader extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference mapReference;
    private final MarkerOptions markerOptions;
    private int defaultResourceImageId;
    public Marker marker;

    public IconDownloader(GoogleMap map, MarkerOptions markerOptions) {
        this.mapReference = new WeakReference(map);
        this.defaultResourceImageId = R.drawable.ic_stop_standard;
        this.markerOptions = markerOptions;
    }

    @Override
    // Actual download method, run in the task thread
    protected Bitmap doInBackground(String... params) {
            // params comes from the execute() call: params[0] is the url.
            return downloadBitmap(params[0]);
    }

    @Override
    // Once the image is downloaded, associates it to the markerOptions
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
                bitmap = null;
        }

        if (this.mapReference != null) {
            GoogleMap map = (GoogleMap) this.mapReference.get();
            if (this.markerOptions != null) {
                if (bitmap != null) {
                    this.markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                } else {
                    this.markerOptions.icon(BitmapDescriptorFactory.fromResource(defaultResourceImageId));
                }
            }
            marker = map.addMarker(markerOptions);
        }

        super.onPostExecute(bitmap);
    }

    static Bitmap downloadBitmap(String url) {
        DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet getRequest = new HttpGet(url);
        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("IconDownloader", "Error " + statusCode
                        + " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    inputStream = entity.getContent();
                    final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                    return bitmap;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or
            // IllegalStateException
            getRequest.abort();
            Log.w("IconDownloader", "Error while retrieving bitmap from " + url);
        } finally {
            if (client != null) {
                //client.close();
            }
        }
        return null;
    }

}