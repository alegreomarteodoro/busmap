package com.tenidev.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.tenidev.busmap.MainActivity;
import com.tenidev.busmap.R;
import com.tenidev.downloader.util.ImageFetcher;
import com.tenidev.entity.Bus;

import java.util.ArrayList;

/**
 * Created by alegreomarteodoro on 19/07/13.
 */
public class DefaultRowAdapter extends BaseAdapter  implements Filterable {

    private Context activity;
    private static LayoutInflater inflater=null;
    private ArrayList<Bus> data;
    private ArrayList<Bus> filteredData;
    private ImageFetcher mImageFetcher;

    public DefaultRowAdapter(Context a, ArrayList l){
        activity = a;
        data = l;
        filteredData = l;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public Object getItemAtPosition(int position){
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public int getCount() {
        return filteredData.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.default_row, null);

        TextView title = (TextView)vi.findViewById(R.id.text); // title
        ImageView thumb_image = (ImageView)vi.findViewById(R.id.icon); // thumb image

        Bus currentBus = (Bus) filteredData.get(position);

        String[] parts = currentBus.getNeighborhood().split("-");
        String description = parts[0].trim() + " ... " + parts[parts.length - 1].trim();
        title.setText(currentBus.getTitle() + " - " + description);

        if (MainActivity.class.isInstance(activity)) {
            mImageFetcher = ((MainActivity) activity).getImageFetcher();
            mImageFetcher.loadImage(currentBus.getIcon(), thumb_image);
        }

        return vi;
    }

    public void setList(ArrayList<Bus> data){
        this.data = data;
        this.filteredData = data;
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence)
            {
                FilterResults results = new FilterResults();

                //If there's nothing to filter on, return the original data for your list
                if(charSequence == null || charSequence.length() == 0)
                {
                    results.values = data;
                    results.count = data.size();
                }
                else
                {
                    filteredData = new ArrayList<Bus>();

                    for(Bus itemData : data)
                    {
                        Boolean cond1 = itemData.getTitle().toLowerCase().contains(charSequence.toString().toLowerCase());
                        Boolean cond2 = itemData.getNeighborhood().toLowerCase().contains(charSequence.toString().toLowerCase());
                        if( cond1 || cond2 ) {

                            filteredData.add(itemData);
                        }
                    }

                    results.values = filteredData;
                    results.count = filteredData.size();
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults)
            {
                filteredData = (ArrayList<Bus>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
